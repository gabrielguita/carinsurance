import React from "react";
import { Router, Switch } from "react-router-dom";
import Login from "./../components/pages/Login";
import Car from "./../components/pages/Car";
import SelectPlan from "./../components/pages/SelectPlan";
import history from "../actions/helpers";
import PrivateRoute from "./privateRoute";
import PublicRoute from "./publicRoute";

const routes = (
  <div>
    <Router history={history}>
      <Switch>
        <PublicRoute restricted={false} component={Login} path="/" exact />
        <PrivateRoute
          restricted={true}
          component={SelectPlan}
          path="/selectplan"
        />
        <PrivateRoute restricted={true} component={Car} path="/car" />
      </Switch>
    </Router>
  </div>
);

export default routes;
