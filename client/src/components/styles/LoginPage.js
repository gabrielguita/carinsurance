import styled from "styled-components";

export const TopBar = styled.div`
  position: fixed;
  top: 0;
  background-image: linear-gradient(122deg, #317bda -6%, #33c3c8);
  width: 100%;
  left: 0;
  height: 26px;
  a {
    left: 20%;
    position: absolute;
    top: 10px;
    color: #fff;
    font-family: Roboto;
    font-size: 9px;
    font-weight: 500;
    letter-spacing: 0.75px;
  }
`;

export const AskAccess = styled.div`
  border-radius: 3px;
  border: solid 1px #fff;
  width: 100%;
  height: 45px;
  font-family: Roboto;
  font-size: 14px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 42px;
  letter-spacing: normal;
  text-align: center;
  color: #fff;
  padding: 0 20px;
  margin: 25px 0;
  span {
    margin-right: 4px;
  }
  a {
    color: #fff;
    text-decoration: underline;
  }
`;

export const LoginContainer = styled.div`
  margin: 90px 0px 0px 0;
  &:before {
    content: "";
    position: fixed;
    background-image: linear-gradient(122deg, #317bda -6%, #33c3c8);
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    z-index: -1;
  }
`;

export const Logo = styled.div`
  width: 86.1px;
  height: 100px;
  object-fit: contain;
  margin: 0 auto 30px auto;
`;

export const ReqAccessSection = styled.div`
  margin: 0 auto;
  width: 350px;
  height: 332px;
`;
