import styled from "styled-components";
import media from "styled-media-query";

export const CarContainer = styled.div`
  margin: 15% 0px 0px 0;
  &:before {
    content: "";
    position: fixed;
    background-image: linear-gradient(122deg, #317bda -6%, #33c3c8);
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    z-index: -1;
  }
`;

export const ReqAccessSection = styled.div`
  width: 935px;
  margin: 0 auto;
  border-radius: 2px;
  box-shadow: 0 2px 4px 0 rgba(72, 72, 72, 0.5);
  background-color: var(--white);
  ${media.lessThan("950px")`
  width: 100%;
`}
`;
