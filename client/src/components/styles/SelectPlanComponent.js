import styled from "styled-components";
import media from "styled-media-query";

export const Row = styled.div`
  padding: 15px 0;
  border-bottom: solid 1px rgba(146, 146, 146, 0.22);
  font-family: Roboto;
  font-size: 12px;
  font-weight: bold;
  letter-spacing: 0.7px;
  text-align: center;
  &.priceRow {
    height: 100px;
    font-size: 38px;
    font-weight: bold;
    line-height: 20px;
    text-transform: uppercase;
  }
`;

export const PlanType = styled.span`
  font-size: 18px;
  font-family: Roboto;
`;
export const Price = styled.div`
  font-size: 38px;
  font-family: Roboto;
  margin: 14px 0 0px 0;
`;

export const Yearly = styled.span`
  font-weight: 100;
  font-size: 11px;
`;

export const LightText = styled.span`
  font-weight: normal;
`;

export const Plan = styled.div`
  width: 323px;
  border-radius: 3px;
  mix-blend-mode: multiply;
  border-radius: 3px;
  box-shadow: 0 2px 4px 0 rgba(72, 72, 72, 0.5);
  float: left;
  &:last-child {
    margin-left: 10px;
    ${media.lessThan("800px")`
      width: 100%;
      float: none;
      margin: 20px 0 0 0;
    `}
  }

  ${media.lessThan("800px")`
    width: 100%;
    float: none;
  `}

  button {
    width: 303px;
    height: 50px;
    border-radius: 5px;
    border: solid 1px #31cfda;
    background-color: ${props => (props.active ? "#fff" : "#31cfda")};
    color: ${props => (props.active ? "#31cfda" : "#fff")};
    font-family: Sourcesanspro, sans-serif;
    font-size: 16px;
    font-weight: bold;
    letter-spacing: 0.7px;
    text-align: center;
    margin: 10px 0;
    position: relative;
    &:focus {
      outline: none;
    }

    &:hover {
      background-color: ${props => (props.active ? "#31cfda" : "#fff")};
      color: ${props => (props.active ? "#fff" : "#000")};
      opacity: 0.8;
    }
  }

  &.activePlan {
    .priceRow {
      background-color: rgba(255, 255, 255, 0.15);
    }
    button {
      &:hover:disabled {
        background: #fff;
        opacity: 1;
        color: #31cfda;
      }
    }
  }

  &.notActivePlan {
    .priceRow {
      background: #f5fdfe;
      color: #31cfda;
    }
  }
`;
