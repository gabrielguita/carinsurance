import styled from "styled-components";

export const Alert = styled.div`
  color: red;
  font-size: 12px;
  text-align: center;
`;

export const Input = {
  width: "100%",
  border: "none",
  paddingBottom: "10",
  background: " transparent"
};

export const Label = styled.div`
  margin-bottom: 20px;
  width: 240px;
  text-align: left;
  height: 11px;
  font-family: Roboto;
  font-size: 10px;
  color: #5b7289;
`;

export const InputRow = styled.div`
  width: 310px;
  border-bottom: solid 2px #317bda;
`;

export const WelcomeTitle = styled.div`
  padding: 10px 0;
  font-family: Roboto;
  font-size: 18px;
  line-height: 1.56;
  text-align: center;
  color: #5b7289;
`;

export const RememberMe = styled.div`
  width: 100px;
  float: left;
  height: 14px;
  font-size: 12px;
  letter-spacing: normal;
  color: #5b7289;
  img {
    margin-bottom: -4px;
  }
`;

export const LoginContainer = styled.div`
  padding: 20px;
  background: #fff;
  width: 100%;
  border: solid 1px #fff;
  text-align: center;
  border-radius: 3px;
  box-shadow: 0 2px 2px 0 #d4dce2;
  background-color: #fff;
  button {
    margin: 40px 0 0 0;
    width: 310px;
    height: 45px;
    border-radius: 3px;
    background-color: #317bda;
    color: #fff;
    font-family: Roboto;
    font-size: 14px;
    font-weight: bold;
    text-align: center;
    &:hover {
      opacity: 0.8;
    }
  }
`;
