import styled from "styled-components";
import backgroundplans from "./../../assets/backgroundplans.svg";

export const PlanContainer = styled.div`
  max-width: 700px;
  margin: 0 auto;
  .activePlan {
    background: #31cfda;
    color: #fff;
  }
  .notActivePlan {
    background: #fff;
    color: #484848;
  }
`;

export const Title = styled.h1`
  color: #fff;
  font-family: "Sourcesanspro", sans-serif;
  font-size: 26px;
  font-weight: 700;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 1px;
  text-align: center;
`;

export const SelectPlanContainer = styled.div`
  margin: 45px 0px 0px 0;
  text-align: center;
  &:before {
    content: "";
    position: fixed;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: url(${backgroundplans}) no-repeat fixed center top;
    z-index: -1;
  }
`;

export const TogglePlan = styled.div`
  margin: 10px auto 80px auto;
  width: 300px;
  color: #fff;
`;

export const Span = styled.div`
  line-height: 34px;
  width: 110px;
  font-family: "Roboto";
  float: left;
  text-transform: uppercase;
  display: inline-block;
  font-weight: ${props => (props.active ? "normal" : "bold")};
`;

export const FullComparisonTable = styled.div`
  font-family: "Roboto";
  font-size: 14px;
  font-weight: bold;
  color: #31cfda;
  clear: both;
  padding: 30px;
`;
