import styled from "styled-components";
import media from "styled-media-query";

export const Label = styled.div`
  font-family: Roboto;
  font-size: 15px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #484848;
  text-align: left;
  float: left;
  width: 140px;
  line-height: 35px;
`;

export const Row = styled.div`
  width: 600px;
  .right {
    display: inline-block;
    padding-bottom: 0px;
    position: relative;
    > div {
      height: 55px;
      display: inline-block;
      position: relative;
      width: 100%;
    }
    .error {
      color: #ee3d57;
      font-size: 14px;
    }
    ${media.lessThan("950px")`
    width: 100%;
  `}
  }
  select,
  input {
    -webkit-appearance: menulist-button;
    height: 40px;
    background: transparent;
    height: 40px;
    border-radius: 2px;
    border: solid 1px rgba(72, 72, 72, 0.2);
    background-color: white;
    float: left;
    width: auto;
    font-size: 15px;
    padding: 0 7px 0px 7px;
    &:focus {
      outline: none;
    }
    &.error {
      color: #ee3d57 !important;
      border: solid 1px #ee3d57;
    }
    ${media.lessThan("375px")`
      width: 100%;
    `}
  }

  select {
    -webkit-appearance: menulist-button;
    width: 324px;
    text-transform: capitalize;
    ${media.lessThan("375px")`
    width: 100%;
  `}
  }

  ${media.lessThan("375px")`
    width: 100%;
    height: auto;
    line-height: 31px;
    margin-bottom: 10px;
  `}
`;

export const CarContainer = styled.div`
  padding: 20px;
  background: #fff;
  width: 100%;
  border: solid 1px #fff;
  textalign: center;
  border-radius: 3px;
  box-shadow: 0 2px 2px 0 #d4dce2;
  background-color: #fff;
  form {
    max-width: 500px;
    margin: 0 auto;
    .getApriceBtn {
      width: 163px;
      height: 50px;
      border-radius: 5px;
      border: solid 1px #31cfda;
      background-color: #31cfda;
      color: #fff;
      font-size: 16px;
      font-weight: bold;
      text-align: center;
      transition: background-color 0.5s ease;
      margin: 20px 0 0px 0;
      ${media.lessThan("375px")`
      width: 100%;
      margin: 20px 0 0px 0;
    `}
    }
    .getApriceBtn:hover {
      opacity: 0.9;
    }
  }
`;
