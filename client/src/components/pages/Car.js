import React, { Component } from "react";
import "./../../App.css";
import showResults from "../../utils/showResults";
import Carform from "./../Carform";
import { CarContainer, ReqAccessSection } from "../styles/CarPage";

class Car extends Component {
  render() {
    return (
      <CarContainer>
        <ReqAccessSection>
          <Carform onSubmit={showResults} />
        </ReqAccessSection>
      </CarContainer>
    );
  }
}

export default Car;
