import React, { Component } from "react";
import logo from "./../../assets/white.svg";
import { Link } from "react-router-dom";
import "./../../App.css";
import LoginForm from "../LoginForm";

import {
  TopBar,
  AskAccess,
  LoginContainer,
  Logo,
  ReqAccessSection
} from "./../styles/LoginPage";

class Login extends Component {
  render() {
    return (
      <LoginContainer>
        <TopBar>
          {" "}
          <Link to="#"> &lt; QOVER.ME</Link>
        </TopBar>
        <Logo>
          <img src={logo} className="App-logo" alt="logo" />
        </Logo>
        <ReqAccessSection>
          <LoginForm />
          <AskAccess>
            <span>Don't have an account?</span>
            <Link to="/">Ask Access</Link>
          </AskAccess>
        </ReqAccessSection>
      </LoginContainer>
    );
  }
}

export default Login;
