import React, { Component } from "react";
import "./../../App.css";
import comparison from "./../../assets/comparison.svg";
import PropTypes from "prop-types";
import SelectPlanComponent from "./../SelectPlanComponent";

import {
  PlanContainer,
  Title,
  SelectPlanContainer,
  TogglePlan,
  Span,
  FullComparisonTable
} from "./../styles/SelectPlanPage";

class SelectPlan extends Component {
  state = {
    selectedPlan: "Pay Monthly",
    selectedCar: {},
    isChecked: false
  };

  static propTypes = {
    location: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = {
      selectedCar: props.location.state,
      selectedPlan: "Pay Monthly"
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange() {
    this.setState({
      isChecked: !this.state.isChecked,
      selectedPlan: !this.state.isChecked ? "Pay Yearly" : "Pay Monthly"
    });
  }

  render() {
    const { isChecked, selectedCar, selectedPlan } = this.state;
    return (
      <SelectPlanContainer>
        <Title>Select a plan</Title>
        <TogglePlan>
          <Span active={isChecked}>Pay Monthly</Span>
          <label className="switch">
            <input
              type="checkbox"
              value={this.state.isChecked}
              onChange={this.handleChange}
            />
            <div className="slider"></div>
          </label>
          <Span active={!isChecked}>Pay Yearly</Span>
        </TogglePlan>
        <PlanContainer className={"selected"}>
          <SelectPlanComponent
            plans={plans}
            selectedCar={selectedCar}
            payment={selectedPlan}
          />
        </PlanContainer>
        <FullComparisonTable>
          Show me the full comparasion table <img src={comparison} alt="" />
        </FullComparisonTable>
      </SelectPlanContainer>
    );
  }
}

const plans = [
  {
    id: 1,
    plantype: "Global",
    travelDays: "90",
    reimbursement: "1000000",
    personalAssistance: "5000",
    travelAssistance: "1000",
    coverage: "1 year"
  },
  {
    id: 2,
    plantype: "Universal",
    travelDays: "180",
    reimbursement: "3000000",
    personalAssistance: "10000",
    travelAssistance: "2500",
    coverage: "1 year"
  }
];

export default SelectPlan;
