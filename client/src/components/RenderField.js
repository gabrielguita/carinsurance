import React from "react";
import styled from "styled-components";

const RenderField = ({
  input,
  label,
  type,
  meta: { touched, error },
  options,
  ...rest
}) => {
  const parse = event => {
    return JSON.parse(event.target.value);
  };
  return (
    <>
      <label>{label}</label>
      {type === "select" && (
        <div>
          <select
            className={touched && error ? "error" : ""}
            onBlur={event => input.onBlur(parse(event))}
            onChange={event => input.onChange(parse(event))}
            {...rest}
          >
            <option value="select">Select a car</option>
            {options.map(data => (
              <option
                key={data.id}
                value={JSON.stringify(data)}
                selected={input.value.id === data.id}
              >
                {data.name}
              </option>
            ))}
          </select>
          {touched && error && <div className="error">{error}</div>}
        </div>
      )}
      {type !== "select" && (
        <div>
          <input
            {...input}
            placeholder={label}
            type={type}
            className={touched && error ? "error" : ""}
          />{" "}
          {input.name === "purchasePrice" && (
            <CurrencySign style={{ color: touched && error ? "red" : "" }}>
              €
            </CurrencySign>
          )}
          {touched && error && <div className="error">{error}</div>}
        </div>
      )}
    </>
  );
};

const CurrencySign = styled.span`
  font-family: Roboto;
  padding-left: 10px;
  line-height: 42px;
`;

export default RenderField;
