import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { login } from "../actions/authActions";
import { Link, withRouter } from "react-router-dom";
import { clearErrors } from "../actions/errorActions";
import { push } from "connected-react-router";
import checkednolabel from "./../assets/checkednolabel.svg";
import {
  Alert,
  Input,
  Label,
  InputRow,
  WelcomeTitle,
  RememberMe,
  LoginContainer
} from "./styles/LoginForm";

class LoginForm extends Component {
  state = {
    email: "",
    password: "",
    msg: null
  };

  static propTypes = {
    isAuthenticated: PropTypes.bool,
    error: PropTypes.object.isRequired,
    login: PropTypes.func.isRequired,
    clearErrors: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
  };

  onChange = e => this.setState({ [e.target.name]: e.target.value });

  onSubmit = e => {
    e.preventDefault();

    const { email, password } = this.state;

    const user = {
      email,
      password
    };

    this.props.login(user);
  };

  render() {
    const { msg } = this.props.auth;
    return (
      <React.Fragment>
        <LoginContainer>
          <WelcomeTitle>Welcome at Qover</WelcomeTitle>
          {msg ? <Alert>{msg}</Alert> : null}
          <form onSubmit={this.onSubmit}>
            <InputRow>
              <Label>Email: </Label>
              <input
                style={Input}
                type="text"
                name="email"
                onChange={this.onChange}
                className="inputElm"
              />
            </InputRow>
            <br />
            <InputRow>
              <Label>Password: </Label>
              <input
                style={Input}
                type="password"
                name="password"
                onChange={this.onChange}
                className="inputElm"
              />
            </InputRow>
            <br />
            <RememberMe>
              <img src={checkednolabel} alt="" /> Remember me
            </RememberMe>
            <Link to="#" className="forgotYourPassword">
              Forgot your password?
            </Link>
            <button type="submit">Sing in to your account</button>
          </form>
        </LoginContainer>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  error: state.error,
  auth: state.auth
});

export default withRouter(
  connect(mapStateToProps, { login, clearErrors, push })(LoginForm)
);
