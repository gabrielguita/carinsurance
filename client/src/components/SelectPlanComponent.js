import React, { Component } from "react";
import "./../App.css";
import styled from "styled-components";
import valid from "./../assets/valid.svg";
import PropTypes from "prop-types";
import {
  PlanType,
  Row,
  Price,
  Yearly,
  LightText,
  Plan
} from "./styles/SelectPlanComponent";

class SelectPlanComponent extends Component {
  static propTypes = {
    logoutUser: PropTypes.object
  };

  constructor(props) {
    super(props);

    this.state = {
      activeIndex: 0,
      isPaymentChecked: props.isPaymentChecked || false,
      selectedPlan: props.plans[0],
      price: props.selectedCar.car.price,
      payment: props.payment
    };

    this.handlePlan = this.handlePlan.bind(this);
  }

  handlePlan = (index, plan, price, payment) => {
    this.setState({
      activeIndex: index,
      selectedPlan: plan,
      price: price,
      payment: payment
    });
  };

  render() {
    const { plans, selectedCar, payment } = this.props;
    const { activeIndex } = this.state;
    const { car, purchasePrice } = selectedCar;
    const globalPrice = car.price;
    const universalPrice =
      parseInt(car.price) + (0.3 / 100) * parseInt(purchasePrice);
    return (
      <div>
        <React.Fragment>
          {plans.map((plan, i) => (
            <Plan
              key={i}
              active={activeIndex === i}
              className={activeIndex === i ? "activePlan" : "notActivePlan"}
            >
              <Row>
                <PlanType>{plan.plantype}</PlanType>
              </Row>
              <Row className="priceRow">
                <Price>
                  {plan.plantype === "Global" && globalPrice}
                  {plan.plantype === "Universal" && universalPrice}
                </Price>
                <Yearly>Yearly incl. taxes</Yearly>
              </Row>
              <Row>
                Maximum duration travel <LightText>of</LightText>{" "}
                {plan.travelDays}
              </Row>
              <Row>
                Medical expreses reimbursement <LightText>up to</LightText>{" "}
                {plan.reimbursement} €
              </Row>
              <Row>
                Personal assistance abroad <LightText>up to</LightText>{" "}
                {plan.personalAssistance}
              </Row>
              <Row>
                Travel assistance abroad <LightText>up to</LightText>{" "}
                {plan.travelAssistance}
                <LightText>per insured per travel</LightText>
              </Row>
              <Row>Coverage duration {plan.coverage} year </Row>
              <button
                type="button"
                onClick={() =>
                  this.handlePlan(
                    i,
                    plan,
                    plan.plantype === "Global" ? globalPrice : universalPrice,
                    payment
                  )
                }
                disabled={activeIndex === i}
              >
                <ValidPlan isActive={activeIndex === i} />
              </button>
            </Plan>
          ))}
        </React.Fragment>
      </div>
    );
  }
}

const ValidPlan = isActive => {
  const Valid = styled.div`
    width: 20px;
    height: 20px;
    position: absolute;
    left: 20%;
    background: url(${isActive ? `${valid}` : ""});
  `;

  return (
    <>
      <Valid active={isActive} />
      {isActive.isActive ? "Plan Selected" : "Choose this plan"}
    </>
  );
};

export default SelectPlanComponent;
