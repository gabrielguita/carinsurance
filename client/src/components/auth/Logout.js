import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import PropTypes from "prop-types";

export class LogoutUser extends Component {
  static propTypes = {
    logoutUser: PropTypes.func.isRequired
  };

  render() {
    return (
      <Fragment>
        <a onClick={this.props.logoutUser} href="#">
          Logout
        </a>
      </Fragment>
    );
  }
}

export default connect(null, { logoutUser })(LogoutUser);
