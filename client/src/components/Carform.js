import React from "react";
import { Field, reduxForm } from "redux-form";
import RenderField from "./RenderField";
import { Label, Row, CarContainer } from "./styles/Carform";

let Carform = props => {
  const { handleSubmit, submitting } = props;
  return (
    <CarContainer>
      <form onSubmit={handleSubmit}>
        <Row>
          <Label>Age of the driver: </Label>
          <div className="right">
            <Field
              placeholder="Type age.."
              name="age"
              type="number"
              component={RenderField}
              validate={[required, number, minValue18]}
            />
          </div>
        </Row>
        <Row>
          <Label>Car:</Label>
          <div className="right">
            <Field
              type="select"
              name="car"
              component={RenderField}
              validate={[required, minValue25ForSportCar]}
              options={carDetails}
            ></Field>
          </div>
        </Row>
        <Row>
          <Label>Purchase Price </Label>
          <div className="right">
            <Field
              placeholder="Type purchase price.."
              name="purchasePrice"
              type="number"
              component={RenderField}
              validate={[required, number, minValue5k]}
            />
          </div>
        </Row>
        <Row>
          <div>
            <button
              type="submit"
              className="getApriceBtn"
              disabled={submitting}
            >
              Get a price
            </button>
          </div>
        </Row>
      </form>
    </CarContainer>
  );
};

const required = value => (value ? undefined : "Required");
const number = value =>
  value && isNaN(Number(value)) ? "Must be a number" : undefined;
const minValue = min => value =>
  value && value < min ? `Must be at least ${min}` : undefined;
const minValue18 = minValue(18);
const minValue25ForSportCar = (car, allvalues) =>
  car.name === "porsche" && allvalues.age < 25 ? "Must be at least 25" : "";
const minValue5k = minValue(5000);

const carDetails = [
  {
    id: 1,
    name: "audi",
    price: 250
  },
  {
    id: 2,
    name: "bmw",
    price: 150
  },
  {
    id: 3,
    name: "porsche",
    price: 500
  }
];

Carform = reduxForm({
  form: "selectCarFormValues"
})(Carform);

export default Carform;
