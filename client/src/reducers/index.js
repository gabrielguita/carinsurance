import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import { errorReducer } from "./errorReducer";
import authReducer from "./authReducer";
import { reducer as reduxFormReducer } from "redux-form";

const createRootReducer = history =>
  combineReducers({
    router: connectRouter(history),
    auth: authReducer,
    error: errorReducer,
    form: reduxFormReducer
  });

export default createRootReducer;
