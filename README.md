# Car Insurance 0.1.0

## Installation

Clone this repo into a new project folder and run install script.

With npm

```sh
$ git clone git@bitbucket.org:gabrielguita/carinsurance.git new-project
$ cd new-project
$ # Install dependencies for server
$ npm install

# Install dependencies for client
$ npm run client-install

# Run the client & server with concurrently
$ npm run dev

# Run the Express server only
$ npm run server

# Run the React client only
$ npm run client

# Server runs on http://localhost:5000 and client on http://localhost:3000


```

Open http://localhost:3000 in browser if doesn't start automatically

## Static assets

Just add to `src/assets/`

<hr />

## ES6

This starter includes [Babel](https://babeljs.io/) so you can directly use ES6 code.

## Changelog

-   0.1.0 - initial upload for Carinsurance
